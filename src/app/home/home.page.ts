import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public folder!: string;
  Slides: any;
  
  

  constructor(private activatedRoute: ActivatedRoute) { }
  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id') as string;
  }
  slideOps = {
    loop: true
  };

  slidesDidLoad(slides: IonSlides) {
    slides.startAutoplay();
  }
  

}
