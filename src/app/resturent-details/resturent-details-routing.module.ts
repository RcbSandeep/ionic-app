import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResturentDetailsPage } from './resturent-details.page';

const routes: Routes = [
  {
    path: '',
    component: ResturentDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResturentDetailsPageRoutingModule {}
